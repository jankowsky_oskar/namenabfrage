import java.util.Scanner;

class Abfrage {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Vorname: ");
        final String firstname = sc.next();

        System.out.print("Nachname: ");
        final String lastname = sc.next();

        System.out.println(firstname + " " + lastname);

        sc.close();

    }

}